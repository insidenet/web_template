# テンプレート・タイプ3

タイムライン表示行うサンプル。

### フォルダ構成
	<template0003>	... タイムライン・サンプル
		config.rb
		<css>
			sample01.css
		<sass>
			_timeline.scss				... タイムライン用SASS本体
			_common_function.scss
			_common_variables.scss
			sample01.scss
		<images>
		<js>
			sample01.js
		<doc>
			wire_frame.pages			... ワイヤー・フレーム
			wire_frame.pdf				... ワイレー・フレーム PDF
			design_comp_center.png		... デザイン・カンプ（中央配置）
			design_comp_left.png		... デザイン・カンプ（左配置）
			design_comp_right.png		... デザイン・カンプ（右配置）

		sample01.html			... タイムライン・サンプル
		
		README.md	... 本ドキュメント

テンプレートの詳細は、ワイヤー・フレームやカンプを参照。  

- - -
## サンプル０１(sample01.html)の要件定義

* タイムラインを簡単に設定できること。
* 初期表示時にはラインをセンターに設置すること。
* レスポンジブにすること。
* 640px以下になるとラインを左側に寄せること。また、右寄せのSASSも用意すること。
* CSSのみで構成できるようにすること。
* 汎用性を持たせるために、タイムラインを構成するSASSはMixinでまとめること。(_timeline.scss）
* 次ページ対応のSASSも準備すること。

### 補足事項
* Animation表示させること。
* クリックイベントに対応させること。
* 時系列にソート可能にすること。（昇順・降順）

- - -
## 必須HTML構成
```
<ul>
	<li>
		<article class="animated">
			<h2>
				年月日
			</h2>
			<div>
				内容
			</div>
		</article>
	</li>
</ul>
```

- - -
## 参考文献

* [TimeLine](http://kumaocky.github.io/portfolio/resume.html)

- - -
## 更新履歴

##### 2017/03/05
* 新規作成

##### 2017/03/09
* ColorBoxに対応

##### 2017/03/10
* Colorbox微調整、縦書き表記に対応
