/*----------------------------------------------------------------------------------------------------------
初期化処理
----------------------------------------------------------------------------------------------------------*/
$(
	function()
	{
		function SetWayPoint( target, context )
		{
			$( target ).waypoint
			(
				{
					handler: function( direction )
					{
						if( direction === 'down' )
						{
							$( this.element ).removeClass("fadeOut");
							$( this.element ).addClass("fadeIn");
						}
						else if( direction === 'up' )
						{
							$( this.element ).removeClass("fadeIn");
							$( this.element ).addClass("fadeOut");
						}
					},
					context: context,
					offset: function()
					{
						//return ( this.element.clientHeight / 2 );
						return $('main').height() * 0.8;
					}
	  			}
			);
		}

		function ScrollUp( target )
		{
			$( target ).animate
			(
				{
					scrollTop: 0
				},
				500,
				'swing'
			);
		}
		function ScrollDown( target )
		{
			$( target ).animate
			(
				{
					scrollTop: $(target)[0].scrollHeight - $(target).height()
				},
				500,
				'swing'
			);
		}

		function SortDown()
		{
			$('.insTimeLine ul').html
			(
				$('.insTimeLine li').sort
				(
					function( a, b )
					{
						return $.trim( $('article h2', a).html() ) > $.trim( $('article h2', b).html() ) ? 1 : -1;
					}
				)
			);
			
			$('.insTimeLine li article').removeClass( 'fadeOut' );
			SetWayPoint( '.insTimeLine .animated', 'main' );
			SetColorBox();
		}
		function SortUp()
		{
			$('.insTimeLine ul').html
			(
				$('.insTimeLine li').sort
				(
					function( a, b )
					{
						return $.trim( $('article h2', a).html() ) < $.trim( $('article h2', b).html() ) ? 1 : -1;
					}
				)
			);
			
			$('.insTimeLine li article').removeClass( 'fadeOut' );
			SetWayPoint( '.insTimeLine .animated', 'main' );
			SetColorBox();
		}
		
		function SetColorBox()
		{
			$('.insTimeLine li').colorbox
			(
				{
					fixed: true,
					inline:true,
					maxWidth:"90%",
					maxHeight:"90%",
					opacity: 0.7,
					rel:'slideshow',
					//slideshow:true,
					//slideshowAuto: true,
					href: function()
					{
						return $(this).attr('data-src');
					},
					onOpen: function()
					{
						$( '.insTimeLine li article' ).css( 'animation', 'none' );
						$( '.insTimeLine li article' ).css( '-webkit-animation', 'none' );
					},
					onClosed: function()
					{
						$( '.insTimeLine li article' ).css( 'animation', '' );
						$( '.insTimeLine li article' ).css( '-webkit-animation', '' );
					}
				}
			);
		}

		// 印刷時以外
		if( !window.matchMedia( 'print' ).matches )
		{
			SetColorBox();

			$('#button_sort').on
			(
				'click',
				function()
				{
					if( $(this).hasClass( 'button_off' ) )
					{
						$(this).removeClass( 'button_off' );
						$(this).addClass( 'button_on' );
						SortUp();
					} else {
						$(this).removeClass( 'button_on' );
						$(this).addClass( 'button_off' );
						SortDown();
					}
				}
			);

			SetWayPoint( '.insTimeLine .animated', 'main' );

			$('main').scroll
			(
				function()
				{
					if( $('main').scrollTop() > $('header').height() )
					{
						$( '.sTopButton' ).css( 'display', 'block' );
						$( '.sBottomButton' ).css( 'display', 'none' );
					} else {
						$( '.sTopButton' ).css( 'display', 'none' );
						$( '.sBottomButton' ).css( 'display', 'block' );
					}
				}
			);

			$( '.sTopButton' ).on
			(
				'click',
				function()
				{
					ScrollUp( 'main' );
				}
			);
	
			$( '.sBottomButton' ).on
			(
				'click',
				function()
				{
					ScrollDown( 'main' );
				}
			);
		}
	}
);
