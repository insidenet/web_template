# WEBサイト構築テンプレート集

WEBサイトを構築する際、品質を保つためのHTML/CSSのテンプレート集。  
SEO対策も考慮されており、CSSはCompass（SASS記法）を用い、ミニマム化され作成されている。

### フォルダ構成
	<external>		... 外部関連
		<sass>
		<css>
		<images>
		<js>
			<css3-mediaqueries-js>	... css3-mediaqueries-js Gitリポジトリ
			<waypoints>				... waypoints.js Gitリポジトリ
			<colorbox.				... colorbox.js Gitリポジトリ

	<template_base>	... 基本テンプレート
		config.rb
		<css>
			index.css
		<sass>
			_common_function.scss
			_common_variables.scss
			index.scss
		<images>
		<js>
		index.html（https://inside-research.com/inside/template/template_base/index.html）

	<template0001>	... テンプレート・タイプ１
		config.rb
		<css>
			index.css
			sample.css
			sample2.css
			sample3.css
		<sass>
			_common_function.scss
			_common_variables.scss
			index.scss
			sample.cscc
			sample2.cscc
			sample3.cscc
		<images>
		<js>
		<doc>
			wire_frame.pages	... ワイヤー・フレーム
			wire_frame.pdf		... ワイレー・フレーム PDF
			design_comp_pc.png	... デザイン・カンプ（PCバージョン）
			design_comp_sp.png	... デザイン・カンプ（SPバージョン）

		index.html（https://inside-research.com/inside/template/template0001/index.html）
		sample.html（https://inside-research.com/inside/template/template0001/sample.html）
		sample2.html（https://inside-research.com/inside/template/template0001/sample2.html）
		sample3.html（https://inside-research.com/inside/template/template0001/sample3.html）
		
		README.md

	<template0002>	... テンプレート・タイプ2
		config.rb
		<css>
			index.css
			sample.css
		<sass>
			_common_function.scss
			_common_variables.scss
			index.scss
			sample.cscc
			sample2.cscc
		<images>
		<js>
			sample.js
			sample2.js
		<doc>
			wire_frame.pages	... ワイヤー・フレーム
			wire_frame.pdf		... ワイレー・フレーム PDF
			design_comp_pc.png	... デザイン・カンプ（PCバージョン）
			design_comp_sp.png	... デザイン・カンプ（SPバージョン）

		index.html（https://inside-research.com/inside/template/template0002/index.html）
		sample.html（https://inside-research.com/inside/template/template0002/sample.html）
		sample2.html（https://inside-research.com/inside/template/template0002/sample2.html）
		
		README.md

	<template0003>	... タイムライン・サンプル（https://inside-research.com/inside/template/template0003/sample01.html）
		config.rb
		<css>
			sample01.css
		<sass>
			_timeline.scss				... タイムライン用SASS本体
			_common_function.scss
			_common_variables.scss
			sample01.scss
		<images>
		<js>
			sample01.js
		<doc>
			wire_frame.pages			... ワイヤー・フレーム
			wire_frame.pdf				... ワイレー・フレーム PDF
			design_comp_center.png		... デザイン・カンプ（中央配置）
			design_comp_left.png		... デザイン・カンプ（左配置）
			design_comp_right.png		... デザイン・カンプ（右配置）

		sample01.html			... タイムライン・サンプル
		
		README.md	... 本ドキュメント

	README.md

### テンプレートの説明
- template_base
	- 最も基本的なテンプレート
- template0001
	- テンプレート・タイプ１
- template0002
	- テンプレート・タイプ2（Waypoint.js + Animate.css）
- template0003
	- 様々なテクニックを使用するサンプル集。

テンプレートは、HTMLの定義とSASSの定義は、必ず一致するように作成されている。  
正しくフレームを作成することで、表示崩れが起こらないページの作成が可能となる。  
各ブロックは、必ず100%計算した上でサイズを設定すること！  

## SASSファイルに関して
SASSファイルは、Compass（SASS記法）を用いて作成されている。  
修正するには、それぞれのテンプレート・フォルダに移動して、下記コマンドを実行する。

```
cd ./template0001
compass compile --output-style compressed
```

## **利用上の注意事項**

```
{% %}
```

で囲まれた部分は書き換えること！
この部分に関しては、PHP等のプログラムで書き換えることを前提に記載されている。

クローンを作成する場合には、下記コマンドを実行し、css3-mediaqueries.jsを取り込む必要がある。

```
// クローンを作成
git clone https://insidenet@bitbucket.org/insidenet/web_template.git

// パスを移動
cd ./web_template

// サブモジュール環境を初期化
git submodule init

// サブモジュールを更新
git submodule update
```

## 参考文献

* [css3-mediaqueries.js](https://github.com/livingston/css3-mediaqueries-js.git)
* [Sass, Compassの利用方法](https://inside-research.com/2514/css-compass%e3%81%ae%e5%88%a9%e7%94%a8%e6%96%b9%e6%b3%95)
* [SASS/Compassの使用に関して](https://inside-research.com/2640/css-sasscompass%e3%81%ab%e9%96%a2%e3%81%97%e3%81%a6)
* [CSS 適用優先順位の詳細](https://inside-research.com/1921/css-%e9%81%a9%e7%94%a8%e5%84%aa%e5%85%88%e9%a0%86%e4%bd%8d%e3%81%ae%e8%a9%b3%e7%b4%b0)
* [Sass / Compass インストール方法](https://inside-research.com/1173/sass-compass-%e3%82%a4%e3%83%b3%e3%82%b9%e3%83%88%e3%83%bc%e3%83%ab%e6%96%b9%e6%b3%95)

- - -
## 更新履歴

##### 2017/02/13
* 新規作成

##### 2017/02/15
* Compassをbody部から定義できるように修正

##### 2017/02/16
* 新規テンプレートを追加
* ディレクトリ構成を変更

##### 2017/02/19
* template0001にワイヤー・フレーム定義書を追加
* 汎用関数を追加
* template0001にワイヤー・フレーム、カンプに従ったサンプルを追加
* template0001にsample2を追加
* template0001にsample3を追加
* 新しいテンプレート template0002を追加

##### 2017/02/20
* 全体的に、HTML ヘッダー内のイメージツールバーの設定を削除（HTML5 構文エラーとなるため）

##### 2017/02/21
* 全てのサンプルのヘッダーを修正

##### 2017/02/22
* テンプレートの構文エラーを修正

##### 2017/02/24
* 共通関数にBOX中央配置関数を追加

##### 2017/03/04
* template0002 印刷時にも美しく印刷できるように修正
* 全テンプレートのViewport定義を修正（印刷に耐えうるように）
* template0002 不要なCSS属性を削除
* template0002 上下ナビゲーションボタンのHTML定義をbuttonに変更
* template0002にサンプル２を追加

##### 2017/03/05
* template0003 新規作成

##### 2017/03/08
* calc対応background-position関数を追加

##### 2017/03/10
* template0003 Colorbox微調整、縦書き表記に対応

##### 2017/03/12
* 共通関数にリセット補助関数を追加

##### 2017/03/13
* 共通関数に画像データURIスキーム変換関数を追加
