# テンプレート・タイプ２

Waypoint.js、Animate.cssを使用して、動きのあるページを作成するためのテンプレート。

### フォルダ構成
	<template0002>	... テンプレート・タイプ２
		config.rb
		<css>
			index.css
			sample.css
		<sass>
			_common_function.scss
			_common_variables.scss
			index.scss
			sample.scss
			sample2.scss
		<images>
		<js>
			sample.js
			sample2.js
		<doc>
			wire_frame.pages	... ワイヤー・フレーム
			wire_frame.pdf		... ワイレー・フレーム PDF
			design_comp_pc.png	... デザイン・カンプ（PCバージョン）
			design_comp_sp.png	... デザイン・カンプ（SPバージョン）

		index.html
		sample.html
		sample2.html
		
		README.md	... 本ドキュメント

テンプレートの詳細は、ワイヤー・フレームやカンプを参照。  

- - -
## サンプル１(sample.html)の要件定義

可能な限り変数を定義し、定義されたフレーム内でサイズの変更を可能にすること。  
無駄なコードは一切記述しないこと。  
CSS3以降を使用する場合には、必ずCompassでラッピングすること。  
SASSにベンダープレフィックスを記述するときは、Compassで提供されていない場合に限ること。  
その場合には、Mixinでラップすること（決して直書きしない。可能であれば、_common_function.scssに追記すること）。  

#### PC表示時
* ヘッダーは固定すること。
* １ページ内に縦方向がフィットするように作成されること。  
* ページの横幅は最大値で固定されること。  
  但し、固定値よりも小さくなった場合には、レスポンシブとなること。
* その他の項目に関しては、カンプに従うこと。

#### モバイル表示時
* ヘッダーは固定すること。
* １ページ内に縦方向がフィットするように作成されること。  
* ページの横幅は、デバイスの横幅とする。
* カンプに従うこと。

#### 印刷時
* ヘッダーやフッターが重複して印刷されず、理想通り、ページに収まるようにすること。

- - -
## サンプル２(sample2.html)の要件定義
* ヘッダー・フッターを固定させること以外は、サンプル１と同じ。

- - -
## 更新履歴

##### 2017/02/19
* 新規作成

##### 2017/02/24
* 共通関数にBOX中央配置関数を追加

##### 2017/03/04
* 各種コメント追加・修正
* 印刷時にも美しく印刷できるように修正
* 不要なCSS属性を削除
* 上下ナビゲーションボタンのHTML定義をbuttonに変更
* sample2を追加
