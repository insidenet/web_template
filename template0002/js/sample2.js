/*----------------------------------------------------------------------------------------------------------
初期化処理
----------------------------------------------------------------------------------------------------------*/
$(
	function()
	{
		// 印刷時以外
		if( !window.matchMedia( 'print' ).matches )
		{
			$('.animated').waypoint
			(
				{
					handler: function( direction )
					{
						if( direction === 'down' )
						{
							$( this.element ).removeClass("fadeOut");
							$( this.element ).addClass("fadeIn");
						}
						else if( direction === 'up' )
						{
							$( this.element ).removeClass("fadeIn");
							$( this.element ).addClass("fadeOut");
						}
					},
					context: 'main',
					offset: function()
					{
						return ( this.element.clientHeight / 2 );
					}
	  			}
			);

			$('main').scroll
			(
				function()
				{
					if( $('main').scrollTop() > $('header').height() )
					{
						$( '.sTopButton' ).css( 'display', 'block' );
						$( '.sBottomButton' ).css( 'display', 'none' );
					} else {
						$( '.sTopButton' ).css( 'display', 'none' );
						$( '.sBottomButton' ).css( 'display', 'block' );
					}
				}
			);

			$( '.sTopButton' ).on
			(
				'click',
				function()
				{
					$('main').animate
					(
						{
							scrollTop: 0
						},
						500,
						'swing'
					);
				}	
			);
	
			$( '.sBottomButton' ).on
			(
				'click',
				function()
				{
					$('main').animate
					(
						{
							scrollTop: $('main')[0].scrollHeight - $('main').height()
						},
						500,
						'swing'
					);
				}	
			);
		}
	}
);
