/*----------------------------------------------------------------------------------------------------------
初期化処理
----------------------------------------------------------------------------------------------------------*/
$(
	function()
	{
		// 印刷時以外
		if( !window.matchMedia( 'print' ).matches )
		{
			$('.animated').waypoint
			(
				{
					handler: function( direction )
					{
						if( direction === 'down' )
						{
							$( this.element ).removeClass("fadeOut");
							$( this.element ).addClass("fadeIn");
						}
						else if( direction === 'up' )
						{
							$( this.element ).removeClass("fadeIn");
							$( this.element ).addClass("fadeOut");
						}
					},
					offset: function()
					{
						return ( this.element.clientHeight / 2 ) + $('header').height();
					}
	  			}
			);

			$(window).scroll
			(
				function()
				{
					if( $(window).scrollTop() > $('header').height() )
					{
						$( '.sTopButton' ).css( 'display', 'block' );
						$( '.sBottomButton' ).css( 'display', 'none' );
					} else {
						$( '.sTopButton' ).css( 'display', 'none' );
						$( '.sBottomButton' ).css( 'display', 'block' );
					}
				}
			);

			$( '.sTopButton' ).on
			(
				'click',
				function()
				{
					$('html,body').animate
					(
						{
							scrollTop: 0
						},
						500,
						'swing'
					);
				}	
			);
	
			$( '.sBottomButton' ).on
			(
				'click',
				function()
				{
					$('html,body').animate
					(
						{
							scrollTop: $(document).height() - $(window).height()
						},
						500,
						'swing'
					);
				}	
			);
		}
	}
);
